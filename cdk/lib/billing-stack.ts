import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import {Bucket, BucketAccessControl, CorsRule, HttpMethods} from "aws-cdk-lib/aws-s3";
import * as path from "path";
import {BucketDeployment, Source} from "aws-cdk-lib/aws-s3-deployment";
import {
  CacheHeaderBehavior,
  CachePolicy,
  Distribution,
  OriginAccessIdentity,
  ResponseHeadersPolicy
} from "aws-cdk-lib/aws-cloudfront";
import {S3Origin} from "aws-cdk-lib/aws-cloudfront-origins";

export class UIBillingViteExample extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Set CORS policy for S3 bucket
    const s3CorsPolicy: CorsRule = {
      allowedMethods: [HttpMethods.GET, HttpMethods.HEAD],
      allowedOrigins: ['*'],
      allowedHeaders: ['*'],
      exposedHeaders: ['ETag'],
      id: 'cors-policy-id'
    };

    // Create S3 bucket for hosting static assets
    const bucket = new Bucket(this, 'UIBillingViteBucket', {
      accessControl: BucketAccessControl.PRIVATE,
      cors: [s3CorsPolicy]
    })

    // Allow CloudFront to read from S3 bucket
    const originAccessIdentity = new OriginAccessIdentity(this, 'OriginAccessIdentity');
    bucket.grantRead(originAccessIdentity);

    // Allow CORS headers to be passed to S3
    const cachePolicy = new CachePolicy(this, "CachePolicy", {
      headerBehavior: CacheHeaderBehavior.allowList(
          "Origin",
          "Access-Control-Request-Method",
          "Access-Control-Request-Headers"),
    })

    // Create the CloudFront distribution
    const distribution = new Distribution(this, 'UIBillingViteDistribution', {
      defaultRootObject: 'index.html',
      defaultBehavior: {
        origin: new S3Origin(bucket, {originAccessIdentity}),
        responseHeadersPolicy: ResponseHeadersPolicy.CORS_ALLOW_ALL_ORIGINS,
        cachePolicy: cachePolicy
      },
    })

    // Upload Nuxt static artifacts to S3 Bucket
    new BucketDeployment(this, 'ViteBucketDeployment', {
      destinationBucket: bucket,
      sources: [Source.asset(path.resolve(__dirname, '../../dist'))],
      distribution,
      distributionPaths: ['/index.html']
    })
  }
}
