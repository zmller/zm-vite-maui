"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UIBillingViteExample = void 0;
const cdk = require("aws-cdk-lib");
const aws_s3_1 = require("aws-cdk-lib/aws-s3");
const path = require("path");
const aws_s3_deployment_1 = require("aws-cdk-lib/aws-s3-deployment");
const aws_cloudfront_1 = require("aws-cdk-lib/aws-cloudfront");
const aws_cloudfront_origins_1 = require("aws-cdk-lib/aws-cloudfront-origins");
class UIBillingViteExample extends cdk.Stack {
    constructor(scope, id, props) {
        super(scope, id, props);
        // Set CORS policy for S3 bucket
        const s3CorsPolicy = {
            allowedMethods: [aws_s3_1.HttpMethods.GET, aws_s3_1.HttpMethods.HEAD],
            allowedOrigins: ['*'],
            allowedHeaders: ['*'],
            exposedHeaders: ['ETag'],
            id: 'cors-policy-id'
        };
        // Create S3 bucket for hosting static assets
        const bucket = new aws_s3_1.Bucket(this, 'UIBillingViteBucket', {
            accessControl: aws_s3_1.BucketAccessControl.PRIVATE,
            cors: [s3CorsPolicy]
        });
        // Allow CloudFront to read from S3 bucket
        const originAccessIdentity = new aws_cloudfront_1.OriginAccessIdentity(this, 'OriginAccessIdentity');
        bucket.grantRead(originAccessIdentity);
        // Allow CORS headers to be passed to S3
        const cachePolicy = new aws_cloudfront_1.CachePolicy(this, "CachePolicy", {
            headerBehavior: aws_cloudfront_1.CacheHeaderBehavior.allowList("Origin", "Access-Control-Request-Method", "Access-Control-Request-Headers"),
        });
        // Create the CloudFront distribution
        const distribution = new aws_cloudfront_1.Distribution(this, 'UIBillingViteDistribution', {
            defaultRootObject: 'index.html',
            defaultBehavior: {
                origin: new aws_cloudfront_origins_1.S3Origin(bucket, { originAccessIdentity }),
                responseHeadersPolicy: aws_cloudfront_1.ResponseHeadersPolicy.CORS_ALLOW_ALL_ORIGINS,
                cachePolicy: cachePolicy
            },
        });
        // Upload Nuxt static artifacts to S3 Bucket
        new aws_s3_deployment_1.BucketDeployment(this, 'ViteBucketDeployment', {
            destinationBucket: bucket,
            sources: [aws_s3_deployment_1.Source.asset(path.resolve(__dirname, '../../dist'))],
            distribution,
            distributionPaths: ['/index.html']
        });
    }
}
exports.UIBillingViteExample = UIBillingViteExample;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmlsbGluZy1zdGFjay5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImJpbGxpbmctc3RhY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsbUNBQW1DO0FBRW5DLCtDQUFzRjtBQUN0Riw2QkFBNkI7QUFDN0IscUVBQXVFO0FBQ3ZFLCtEQU1vQztBQUNwQywrRUFBNEQ7QUFFNUQsTUFBYSxvQkFBcUIsU0FBUSxHQUFHLENBQUMsS0FBSztJQUNqRCxZQUFZLEtBQWdCLEVBQUUsRUFBVSxFQUFFLEtBQXNCO1FBQzlELEtBQUssQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBRXhCLGdDQUFnQztRQUNoQyxNQUFNLFlBQVksR0FBYTtZQUM3QixjQUFjLEVBQUUsQ0FBQyxvQkFBVyxDQUFDLEdBQUcsRUFBRSxvQkFBVyxDQUFDLElBQUksQ0FBQztZQUNuRCxjQUFjLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFDckIsY0FBYyxFQUFFLENBQUMsR0FBRyxDQUFDO1lBQ3JCLGNBQWMsRUFBRSxDQUFDLE1BQU0sQ0FBQztZQUN4QixFQUFFLEVBQUUsZ0JBQWdCO1NBQ3JCLENBQUM7UUFFRiw2Q0FBNkM7UUFDN0MsTUFBTSxNQUFNLEdBQUcsSUFBSSxlQUFNLENBQUMsSUFBSSxFQUFFLHFCQUFxQixFQUFFO1lBQ3JELGFBQWEsRUFBRSw0QkFBbUIsQ0FBQyxPQUFPO1lBQzFDLElBQUksRUFBRSxDQUFDLFlBQVksQ0FBQztTQUNyQixDQUFDLENBQUE7UUFFRiwwQ0FBMEM7UUFDMUMsTUFBTSxvQkFBb0IsR0FBRyxJQUFJLHFDQUFvQixDQUFDLElBQUksRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3BGLE1BQU0sQ0FBQyxTQUFTLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUV2Qyx3Q0FBd0M7UUFDeEMsTUFBTSxXQUFXLEdBQUcsSUFBSSw0QkFBVyxDQUFDLElBQUksRUFBRSxhQUFhLEVBQUU7WUFDdkQsY0FBYyxFQUFFLG9DQUFtQixDQUFDLFNBQVMsQ0FDekMsUUFBUSxFQUNSLCtCQUErQixFQUMvQixnQ0FBZ0MsQ0FBQztTQUN0QyxDQUFDLENBQUE7UUFFRixxQ0FBcUM7UUFDckMsTUFBTSxZQUFZLEdBQUcsSUFBSSw2QkFBWSxDQUFDLElBQUksRUFBRSwyQkFBMkIsRUFBRTtZQUN2RSxpQkFBaUIsRUFBRSxZQUFZO1lBQy9CLGVBQWUsRUFBRTtnQkFDZixNQUFNLEVBQUUsSUFBSSxpQ0FBUSxDQUFDLE1BQU0sRUFBRSxFQUFDLG9CQUFvQixFQUFDLENBQUM7Z0JBQ3BELHFCQUFxQixFQUFFLHNDQUFxQixDQUFDLHNCQUFzQjtnQkFDbkUsV0FBVyxFQUFFLFdBQVc7YUFDekI7U0FDRixDQUFDLENBQUE7UUFFRiw0Q0FBNEM7UUFDNUMsSUFBSSxvQ0FBZ0IsQ0FBQyxJQUFJLEVBQUUsc0JBQXNCLEVBQUU7WUFDakQsaUJBQWlCLEVBQUUsTUFBTTtZQUN6QixPQUFPLEVBQUUsQ0FBQywwQkFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQzlELFlBQVk7WUFDWixpQkFBaUIsRUFBRSxDQUFDLGFBQWEsQ0FBQztTQUNuQyxDQUFDLENBQUE7SUFDSixDQUFDO0NBQ0Y7QUFqREQsb0RBaURDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgY2RrIGZyb20gJ2F3cy1jZGstbGliJztcbmltcG9ydCB7IENvbnN0cnVjdCB9IGZyb20gJ2NvbnN0cnVjdHMnO1xuaW1wb3J0IHtCdWNrZXQsIEJ1Y2tldEFjY2Vzc0NvbnRyb2wsIENvcnNSdWxlLCBIdHRwTWV0aG9kc30gZnJvbSBcImF3cy1jZGstbGliL2F3cy1zM1wiO1xuaW1wb3J0ICogYXMgcGF0aCBmcm9tIFwicGF0aFwiO1xuaW1wb3J0IHtCdWNrZXREZXBsb3ltZW50LCBTb3VyY2V9IGZyb20gXCJhd3MtY2RrLWxpYi9hd3MtczMtZGVwbG95bWVudFwiO1xuaW1wb3J0IHtcbiAgQ2FjaGVIZWFkZXJCZWhhdmlvcixcbiAgQ2FjaGVQb2xpY3ksXG4gIERpc3RyaWJ1dGlvbixcbiAgT3JpZ2luQWNjZXNzSWRlbnRpdHksXG4gIFJlc3BvbnNlSGVhZGVyc1BvbGljeVxufSBmcm9tIFwiYXdzLWNkay1saWIvYXdzLWNsb3VkZnJvbnRcIjtcbmltcG9ydCB7UzNPcmlnaW59IGZyb20gXCJhd3MtY2RrLWxpYi9hd3MtY2xvdWRmcm9udC1vcmlnaW5zXCI7XG5cbmV4cG9ydCBjbGFzcyBVSUJpbGxpbmdWaXRlRXhhbXBsZSBleHRlbmRzIGNkay5TdGFjayB7XG4gIGNvbnN0cnVjdG9yKHNjb3BlOiBDb25zdHJ1Y3QsIGlkOiBzdHJpbmcsIHByb3BzPzogY2RrLlN0YWNrUHJvcHMpIHtcbiAgICBzdXBlcihzY29wZSwgaWQsIHByb3BzKTtcblxuICAgIC8vIFNldCBDT1JTIHBvbGljeSBmb3IgUzMgYnVja2V0XG4gICAgY29uc3QgczNDb3JzUG9saWN5OiBDb3JzUnVsZSA9IHtcbiAgICAgIGFsbG93ZWRNZXRob2RzOiBbSHR0cE1ldGhvZHMuR0VULCBIdHRwTWV0aG9kcy5IRUFEXSxcbiAgICAgIGFsbG93ZWRPcmlnaW5zOiBbJyonXSxcbiAgICAgIGFsbG93ZWRIZWFkZXJzOiBbJyonXSxcbiAgICAgIGV4cG9zZWRIZWFkZXJzOiBbJ0VUYWcnXSxcbiAgICAgIGlkOiAnY29ycy1wb2xpY3ktaWQnXG4gICAgfTtcblxuICAgIC8vIENyZWF0ZSBTMyBidWNrZXQgZm9yIGhvc3Rpbmcgc3RhdGljIGFzc2V0c1xuICAgIGNvbnN0IGJ1Y2tldCA9IG5ldyBCdWNrZXQodGhpcywgJ1VJQmlsbGluZ1ZpdGVCdWNrZXQnLCB7XG4gICAgICBhY2Nlc3NDb250cm9sOiBCdWNrZXRBY2Nlc3NDb250cm9sLlBSSVZBVEUsXG4gICAgICBjb3JzOiBbczNDb3JzUG9saWN5XVxuICAgIH0pXG5cbiAgICAvLyBBbGxvdyBDbG91ZEZyb250IHRvIHJlYWQgZnJvbSBTMyBidWNrZXRcbiAgICBjb25zdCBvcmlnaW5BY2Nlc3NJZGVudGl0eSA9IG5ldyBPcmlnaW5BY2Nlc3NJZGVudGl0eSh0aGlzLCAnT3JpZ2luQWNjZXNzSWRlbnRpdHknKTtcbiAgICBidWNrZXQuZ3JhbnRSZWFkKG9yaWdpbkFjY2Vzc0lkZW50aXR5KTtcblxuICAgIC8vIEFsbG93IENPUlMgaGVhZGVycyB0byBiZSBwYXNzZWQgdG8gUzNcbiAgICBjb25zdCBjYWNoZVBvbGljeSA9IG5ldyBDYWNoZVBvbGljeSh0aGlzLCBcIkNhY2hlUG9saWN5XCIsIHtcbiAgICAgIGhlYWRlckJlaGF2aW9yOiBDYWNoZUhlYWRlckJlaGF2aW9yLmFsbG93TGlzdChcbiAgICAgICAgICBcIk9yaWdpblwiLFxuICAgICAgICAgIFwiQWNjZXNzLUNvbnRyb2wtUmVxdWVzdC1NZXRob2RcIixcbiAgICAgICAgICBcIkFjY2Vzcy1Db250cm9sLVJlcXVlc3QtSGVhZGVyc1wiKSxcbiAgICB9KVxuXG4gICAgLy8gQ3JlYXRlIHRoZSBDbG91ZEZyb250IGRpc3RyaWJ1dGlvblxuICAgIGNvbnN0IGRpc3RyaWJ1dGlvbiA9IG5ldyBEaXN0cmlidXRpb24odGhpcywgJ1VJQmlsbGluZ1ZpdGVEaXN0cmlidXRpb24nLCB7XG4gICAgICBkZWZhdWx0Um9vdE9iamVjdDogJ2luZGV4Lmh0bWwnLFxuICAgICAgZGVmYXVsdEJlaGF2aW9yOiB7XG4gICAgICAgIG9yaWdpbjogbmV3IFMzT3JpZ2luKGJ1Y2tldCwge29yaWdpbkFjY2Vzc0lkZW50aXR5fSksXG4gICAgICAgIHJlc3BvbnNlSGVhZGVyc1BvbGljeTogUmVzcG9uc2VIZWFkZXJzUG9saWN5LkNPUlNfQUxMT1dfQUxMX09SSUdJTlMsXG4gICAgICAgIGNhY2hlUG9saWN5OiBjYWNoZVBvbGljeVxuICAgICAgfSxcbiAgICB9KVxuXG4gICAgLy8gVXBsb2FkIE51eHQgc3RhdGljIGFydGlmYWN0cyB0byBTMyBCdWNrZXRcbiAgICBuZXcgQnVja2V0RGVwbG95bWVudCh0aGlzLCAnVml0ZUJ1Y2tldERlcGxveW1lbnQnLCB7XG4gICAgICBkZXN0aW5hdGlvbkJ1Y2tldDogYnVja2V0LFxuICAgICAgc291cmNlczogW1NvdXJjZS5hc3NldChwYXRoLnJlc29sdmUoX19kaXJuYW1lLCAnLi4vLi4vZGlzdCcpKV0sXG4gICAgICBkaXN0cmlidXRpb24sXG4gICAgICBkaXN0cmlidXRpb25QYXRoczogWycvaW5kZXguaHRtbCddXG4gICAgfSlcbiAgfVxufVxuIl19