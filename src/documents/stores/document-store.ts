import {defineStore} from "pinia";
import { watch, ref, computed } from 'vue'
import axios from "axios";
import {useRoute} from 'vue-router';


export const useDocumentStore = defineStore('documentStore', () => {

    function defaultSelection(list, selected) {
        watch(list, (newList) => {
            if(newList && newList.length > 0) selected.value = newList[0].value;
        })
    }

    const documentTypes = ref([]);
    const selectedDocumentType = ref(undefined);
    defaultSelection(documentTypes, selectedDocumentType)

    const taxYears = ref([])
    const selectedTaxYear = ref(undefined)
    defaultSelection(taxYears, selectedTaxYear)

    const yearTypes = ref([])
    const selectedYearType = ref(undefined)
    defaultSelection(yearTypes, selectedYearType)

    const calendarYears = ref([])
    const academicYears = ref([])
    const selectedYear = ref(undefined)
    const years = computed(() => selectedYearType.value === "Calendar" ? calendarYears.value : academicYears.value);
    defaultSelection(years, selectedYear)

    const agencies = ref([]);
    const selectedAgency = ref(undefined);
    const comments = ref(undefined);
    defaultSelection(agencies, selectedAgency)

    const sessions = ref([])
    const selectedSession = ref(undefined)
    defaultSelection(sessions, selectedSession)

    const router = useRoute()
    console.log(router.query.id);
    const id = computed(() => router.query.id)
    watch(id, newId =>
        axios.get('/maui/person/inspect/billing/documents.page?getDocumentInfo=1&id=' + newId).then(
        (resp) => {
            const info = resp.data
            documentTypes.value = info.documentTypes
            taxYears.value = info.taxYears
            yearTypes.value = info.yearTypes
            calendarYears.value = info.calendarYears
            academicYears.value = info.academicYears
            agencies.value = info.agencies
            sessions.value = info.sessions
        }
    ))

    return {
        id,
        documentTypes,
        selectedDocumentType,
        taxYears,
        selectedTaxYear,
        yearTypes,
        selectedYearType,
        selectedYear,
        years,
        agencies,
        selectedAgency,
        comments,
        sessions,
        selectedSession
    }
})
