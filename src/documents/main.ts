import { createApp } from 'vue'
import DocumentsPage from './DocumentsPage.vue'
import { createPinia } from 'pinia'
import {createRouter, createWebHistory} from "vue-router";

import '@ais-public/ais-components/tailwind.css'
import '@ais-public/ais-components/uids-fonts.css'


const pinia = createPinia();
const router = createRouter({
    history: createWebHistory(),
    routes: []
});

createApp(DocumentsPage).use(pinia).use(router).mount('#app')
