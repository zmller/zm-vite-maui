import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { fileURLToPath } from 'url'
import {resolve} from 'path'

const root = resolve(__dirname, 'src')
const outDir = resolve(__dirname, 'dist')


// https://vitejs.dev/config/
export default defineConfig({
  root,
  plugins: [vue()],
  build: {
    outDir,
    emptyOutDir: true,
    rollupOptions: {
      input: {
        main: resolve(root, 'index.html'),
        documents: resolve(root, 'documents', 'documents.html')
      },
      output: {
        entryFileNames: `assets/[name]/[name].js`,
        chunkFileNames: `assets/[name]/[name].js`,
        assetFileNames: `assets/[name]/[name].[ext]`
      }
    },
  },
  experimental: {
    renderBuiltUrl(filename: string, { hostId, hostType, type }: { hostId: string, hostType: 'js' | 'css' | 'html', type: 'public' | 'asset' }) {
      return 'https://d11yfcgvf6nkp8.cloudfront.net/' + filename
    }
  }
})