## What is this?

Separating the frontend and backend of a website is an architectural pattern where the UI components (Javascript, CSS,
etc.) are developed, bundled, deployed, and delivered separately from the service code (e.g. SpringBoot, Java, Hibernate,
etc). All of the display-related business logic is shifted to the frontend in this architectural pattern and the
server-side business logic slimmed down considerably as a result. Backend code is exposed to frontend code through a
RESTful web interface; this interface creates a light coupling between the two components that is easily extensible to
other frontend applications.

This repository contains a proof of concept that demonstrates this architectural pattern. An example Vue app is created
and built using Vite, the output of the Vite build is uploaded to an AWS S3 bucket and exposed via AWS Cloudfront. The
Vue application is integrated with existing MAUI code via a lightweight `taglib` that mounts the Vue app to a JSP page
that contains the standard MAUI UI layout.

## Why?

The primary goal is to separate logical domains into "[bounded contexts](https://martinfowler.com/bliki/BoundedContext.html)"
that can be independently developed and deployed. Composing the larger MAUI application into bounded contexts will give
developers more ownership, autonomy, and velocity.

Performing this split now will enable teams to integrate with newer technologies (i.e. NPM, Vue, ais-components) at their
own pace. Integrating Vue with JSP enables the organization to upgrade webpages in a piecemeal way where the harder
aspects (i.e. people inspector, tabs, navigation) can be performed independently and at a later date, when the organization
is ready to do so.

## How does this work?

Developers can use Vue and Vite to develop and build frontend applications independent of MAUI. During development, Vite
provides a development server that provides developer-friendly functionality like quick builds and hot reloading. Vite
uses [Rollup](https://rollupjs.org/) during deployment to produce a collection of static assets (javascript, css, html)
that can be accessed through a web browser. 

Although Vite can be used to build single-page applications, it's usage of Rollup allows us to produce multi-page 
applications where Vite will generate multiple collections of static assets. This package utilizes multi-page feature of
Rollup to generate static assets that can be imported into MAUI  through traditional HTML `script` and `link` elements. 

![screenshot](./docs/MauiScreenShot.png)

In the example above the outlined red section contains the Vue App, everything else is standard Maui JSP. This
implementation enables developers to build the "body" of a webpage in Vue without having to duplicate common MAUI
components like the Person Inspector, Tabs, and Navigation panes.

## Why mount Vue to MAUI?

Mounting a Vue app to Maui allows us to implement Maui pages in Vue using ais-components while taking advantage of common
Maui constructs that haven't been implemented in Vue, such as the Person Inspector, Tabs, left-hand navigation,
and much more. Mounting to MAUI delays the burden of needing to recreate these common components in ais-components (or
another library). It enables developers and teams to upgrade pieces of MAUI slowly, overtime, as dictated by their
teams capacity and velocity.

## How does a Vue app communicate with MAUI?

Vue app can communicate with MAUI's backend by making normal HTTP requests. There is no need for extra
authentication/authorization because the requests will contain MAUI's normal authentication cookies.

There's currently a limitation where Vue apps must use `.page` endpoints that return a `StreamingResolution` because
MAUI's API only authorizes requests with `Bearer` or `Basic` Authorization headers. However, support can be added to the
API to [accept authentication cookies](https://gitlab.com/uiowa-ais/java/maui/-/commit/5f0aa57d2583eb16c86798dc0476d4d8a51514e9)
which would enable Vue apps to call MAUI's API directly. If we  enforce that all Vue apps call MAUI's API -- instead
of `.page` endpoints -- then we will further decouple frontend and backend codebase while also moving MAUI towards a
more modern architecture.

## How does routing work?

There really is no "routing" taking place when MAUI consumes assets built by Vite. In `vite.config.ts` we define Rollup
options that tell Vite to produce multiple collections of static assets, these assets are then imported directly into
MAUI through HTML `script` and `link` elements. 

This means that each time we want to create a new page, we need to add new config to `vite.config.ts`. This has the
potential to negatively impact the developer experience, because it's almost guaranteed that adding this config will
result in merge conflicts when more than one developer are creating new pages.

## How do you mount a Vue app to MAUI?

Vite apps need to be integrated into MAUI in two separate, one for development and the other for production. Vite provides
a server for development -- integrating with this server enables all the developer friendly functionality such as hot
reloading. However, the server doesn't generate static assets but will build the assets automatically when the root
`index.html` page is requested. In order to retrieve the assets and integrate them into MAUI during development, we need
to parse the `index.html` page to find any `script` or `link` elements in order to include them in the MAUI page. 

Integrating Vite assets into MAUI is significantly easier for production use cases. Since Rollup produces static assets, 
all we need to do is import the assets directly into MAUI. Rollup will produce assets with a random hash in the file
name by default, but configuration in `vite.config.ts` can be added to remove the hash, making it easier to import the
assets in MAUI because they have a known, static name. 

I've wrapped all of this logic in a `taglib` so it can be shared and abstracted ([code](https://gitlab.com/uiowa-ais/java/maui/-/blob/BILL-3242-UsingVite/web/WEB-INF/tags/billing/vite-app.tag)).
Otherwise, the actual integration is quite simple since all you need to do is import the tag ([code](https://gitlab.com/uiowa-ais/java/maui/-/blob/BILL-3242-UsingVite/web/WEB-INF/pages/billing/accounts/shared/billing-account-documents.jsp)).

## How's the development experience?

Inside the `taglib` mentioned in the previous section there is some logic to determine if sources should be retrieved
from the local Vite server or a production/staging endpoint. When developing using sources from Vite's server you get
all the developer niceties, such as hot reloading.

There are, however, some issues with development, particularly related to CSS. MAUI uses Tailwind and a few other
CSS files for branding, these files (particularly Tailwind) are not guaranteed to have the same version or same styling
as the same assets imported in the Vue app. If there is any clashes then the browser will opt to use the Vue app's styling
over MAUI's.  This can result in the styling of MAUI to change when the Vue app is mounted.

This CSS issue can be fixed -- I believe -- by bundling MAUI's CSS files into an NPM package and importing that package
as a dev dependency in the Vue app. Developers will get the benefit of auto-complete and intellisense during development,
but the actual assets will not be included with the built artifacts which means MAUI's styling will not be overridden.

## How to deploy?

Static Vue apps can be deployed by loading the `dist` directory to a CDN. MAUI can load these assets to a CDN and
mount the Vue app to the webpage.

An example is provided in the `cdk` directory. The `lib/billing-stack.ts` file contains am AWS CDK application that
creates an S3 bucket and CloudFront distribution that acts as an edge cache for the built Vue app. Vite's `dist`
directory is uploaded to the S3 bucket during deployment of the CDK application and the CloudFront distribution's cache
is invalidated.

The CloudFront distribution is added to the `vite.config.ts` file so that the application references the CDN when fetching
assets.

```
renderBuiltUrl(filename: string, { hostId, hostType, type }: { hostId: string, hostType: 'js' | 'css' | 'html', type: 'public' | 'asset' }) {
    return 'https://d11yfcgvf6nkp8.cloudfront.net/' + filename
}
```

The CloudFront distribution can be added to MAUI's configuration files so that the `taglib` can reference the correct
CDN to mount the Vue app. CloudFront distributions can be created per environment (i.e. billing-test, dev,
test, production) to enable testing and user acceptance before deployment.